# IndividualProject4-sm957


### Requirements:
Rust AWS Lambda and Step Functions

- Rust AWS Lambda function
- Step Functions workflow coordinating Lambdas
- Orchestrate data processing pipeline

### Demo link:
link: https://youtu.be/GZOKEOmZ6Dk?si=KgSN8b9F0GEZ4GjU

### Main steps:

1. Create a new Rust project and add all dependencies to Cargo.toml 

    `cargo new <project_name>`

2. Create the functionality in main.rs. In my first lambda function generate_numbers, it generates ten numbers from 0 to 10 randomly; in my second lambda function, it calculates the average of these numbers.

3. Test the functions locally. Use 
    `cargo lambda watch`  to start test.  In another terminal, use `cargo lambda invoke --data-ascii '{}'` to get the result.

4. Build the function

    `cargo lambda build --release`

5. Deploy the function

    `cargo lambda deploy --region <REGION> --iam-role <ROLE_ARN>`
6. After deploying lambda functions to aws lambda, create a State machine in Step functions:
```
{
  "Comment": "A workflow to generate numbers and calculate the average.",
  "StartAt": "GenerateNumbers",
  "States": {
    "GenerateNumbers": {
      "Type": "Task",
      "Resource": "arn:aws:lambda:your-region:your-account-id:function:generate_numbers",
      "Next": "CalculateAverage"
    },
    "CalculateAverage": {
      "Type": "Task",
      "Resource": "arn:aws:lambda:your-region:your-account-id:function:calculate_average",
      "End": true
    }
  }
}

```
7. Execute the state machine to get the result.

### Screenshots:

- Build and deploy lambda functions.

    ![](pic/pic1.png)

- Lambda functions in aws.

    ![](pic/pic2.png)

- Create a State machine in Step functions.

    ![](pic/pic4.png)

- Graph view of my state machine.

    ![](pic/pic3.png)

