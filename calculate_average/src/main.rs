use lambda_runtime::{Error, Context};
use lambda_runtime::handler_fn;
use serde_json::json;
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize)]
struct Input {
    numbers: Vec<u32>,
}

#[derive(Serialize, Deserialize)]
struct Output {
    average: f32,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = handler_fn(calculate);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn calculate(event: Input, _: Context) -> Result<serde_json::Value, Error> {
    let sum: u32 = event.numbers.iter().sum();
    let count = event.numbers.len() as f32;
    let average = sum as f32 / count;
    let output = Output { average };
    Ok(json!({"average" : output}))
}

    


