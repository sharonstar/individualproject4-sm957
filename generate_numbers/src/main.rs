use lambda_runtime::{handler_fn, Context, Error};
use serde_json::json;
use rand::{thread_rng, Rng};
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize)]
struct Output {
    numbers: Vec<u32>,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = handler_fn(generate);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn generate(_: serde_json::Value, _: Context) -> Result<serde_json::Value, Error> {
    let mut rng = thread_rng();
    let numbers: Vec<u32> = (0..10).map(|_| rng.gen_range(0..11)).collect();
    let output = Output { numbers };
    Ok(json!(output))
}